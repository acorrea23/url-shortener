import os
from dotenv import load_dotenv

env = os.environ.get("FLASK_ENV")
mode = os.environ.get("MODE")

if env == 'production':
    load_dotenv('.env.prod')
elif env == 'development':
    load_dotenv('.env.dev')
load_dotenv('.env')

SERVER_NAME = os.environ.get('SERVER_NAME') or None

SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
SQLALCHEMY_TRACK_MODIFICATIONS = False

REDIS_URL = os.environ.get('REDIS_URL') or 'redis://'
REDIS_QUEUE = os.environ.get('REDIS_QUEUE') or 'default'

ADMIN_USERNAME = os.environ.get('ADMIN_USERNAME')
ADMIN_PASSWORD = os.environ.get('ADMIN_PASSWORD')

URL_REGEX = r'^(?:(?:https?|ftp)://)?(?:\S+(?::\S*)?@)?(?:(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:/[^\s]*)?$'