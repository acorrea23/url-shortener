import redis
from rq import Connection, Worker
from flask.cli import FlaskGroup

from url_shortener import create_app
from url_shortener.extensions import db

app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command('run_worker')
def run_worker():
    redis_connection = redis.from_url(app.config['REDIS_URL'])
    with Connection(redis_connection):
        worker = Worker(app.config['REDIS_QUEUE'])
        worker.work()


@cli.command("create_db")
def create_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command("seed_db")
def seed_db():
    pass
    # TODO: implement


if __name__ == "__main__":
    cli()