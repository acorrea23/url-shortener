from flask import render_template, request, redirect, url_for, current_app, jsonify
from url_shortener.extensions import db
from url_shortener.models import Url
from url_shortener.auth import requires_auth
from url_shortener.utils import shorten_url
from url_shortener.main import bp


@bp.route('/<short_code>')
def redirect_to_url(short_code):
    url = Url.query.filter_by(short_code=short_code).first_or_404()
    url.visits += 1
    db.session.commit()

    return redirect(url.original_url, code=303)


@bp.route('/')
@requires_auth
def index():
    return render_template('index.html', title='New Document')


@bp.route('/add_url', methods=['POST'])
@requires_auth
def add_url():
    original_url = request.form['original_url']
    url = shorten_url(original_url)

    if url:
        return render_template('url_added.html', title='Add', original_url=url.original_url, short_code=url.short_code)
    else:
        # TODO: flash message
        return redirect(url_for('main.index'))


@bp.route('/stats')
@bp.route('/stats/<int:page>')
@requires_auth
def stats(page=1):
    # urls = Url.query.order_by(Url.visits.desc()).all()
    urls = Url.query.order_by(Url.visits.desc()).paginate(page, 10, False)

    return render_template('stats.html', title='Stats', urls=urls)  # TODO: pass title names, datatable integration


@bp.route('/api/start-job/single', methods=['POST'])
def start_job_batch_short_url_sms_single():
    body = request.get_json()

    job = current_app.rq_queue.enqueue('url_shortener.tasks.batch_short_url_sms_single', body, job_timeout=1000)

    response_object = {
        "status": "success",
        "data": {
            "job_id": job.get_id()
        }
    }

    return jsonify(response_object), 202


@bp.route('/api/start-job/multi', methods=['POST'])
def start_job_batch_short_url_sms_multi():
    body = request.get_json()

    job = current_app.rq_queue.enqueue('url_shortener.tasks.batch_short_url_sms_multi', body, job_timeout=1000)

    response_object = {
        "status": "success",
        "data": {
            "job_id": job.get_id()
        }
    }

    return jsonify(response_object), 202


@bp.route('/api/get-job/<job_id>', methods=['GET'])
def get_job_status(job_id):
    job = current_app.rq_queue.fetch_job(job_id)

    if job:
        response_object = {
            "status": "success",
            "data": {
                "job_id": job.get_id(),
                "job_enqueued_at": job.enqueued_at,
                "job_started_at": job.started_at,
                "job_ended_at": job.ended_at,
                "job_status": job.get_status(),
                "job_exception": job.exc_info,
                "job_result": job.result,
                "job_progress": job.meta.get('progress', 0)
            },
        }
    else:
        response_object = {"status": "error"}
    return jsonify(response_object)