import string
from datetime import datetime
from random import choices
from url_shortener.extensions import db


class Url(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    original_url = db.Column(db.String(512))
    short_code = db.Column(db.String(7), unique=True)
    visits = db.Column(db.Integer, default=0)
    created_date = db.Column(db.DateTime, default=datetime.now)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.short_code = self.generate_short_code()

    def generate_short_code(self):
        characters = string.digits + string.ascii_letters
        short_code = ''.join(choices(characters, k=7))

        url = self.query.filter_by(short_code=short_code).first()  # TODO: optimize with exist

        if url:
            return self.generate_short_code()

        return short_code



