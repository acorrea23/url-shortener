from flask import url_for
from url_shortener.utils import shorten_url
from rq import get_current_job
from url_shortener import create_app

app = create_app()
app.app_context().push()


def batch_short_url_sms_single(content):
    job = get_current_job()
    print(f'Starting job {job.get_id()}')

    total_records = len(content['Records'])

    for i, record in enumerate(content['Records']):
        original_url = record['urlLarge']
        url = shorten_url(original_url)

        if url:
            record["copySMS"] = record["copySMS"].replace(
                "{shortUrl}",
                url_for("main.redirect_to_url", short_code=url.short_code, _external=True),
            )

        job.meta['progress'] = 100.0 * i / total_records
        job.save_meta()

    job.meta['progress'] = 100
    job.save_meta()

    return content


def batch_short_url_sms_multi(content):
    job = get_current_job()
    print(f'Starting job {job.get_id()}')

    total_records = len(content['Records'])

    for i, record in enumerate(content['Records']):
        url_dict = {}
        for key, original_url in record['urlLarge'].items():
            url = shorten_url(original_url)

            if url:
                url_dict[key] = url_for("main.redirect_to_url", short_code=url.short_code, _external=True)

        record["copySMS"] = record["copySMS"].format(**url_dict)

        job.meta['progress'] = 100.0 * i / total_records
        job.save_meta()

    job.meta['progress'] = 100
    job.save_meta()

    return content
