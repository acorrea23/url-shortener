import re
from flask import current_app
from url_shortener.extensions import db
from url_shortener.models import Url


def shorten_url(original_url):
    if re.findall(current_app.config['URL_REGEX'], original_url):
        if not (original_url.startswith('http://') or original_url.startswith('https://')):
            original_url = "http://" + original_url
        url = Url(original_url=original_url)
        db.session.add(url)
        db.session.commit()

        return url

    else:
        return None