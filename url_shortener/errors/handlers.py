from flask import render_template
from url_shortener.errors import bp


@bp.app_errorhandler(404)
def error_404(error):
    return f'<h2>{error}</h2>', 404


@bp.app_errorhandler(403)
def error_403(error):
    return f'<h2>{error}</h2>', 403


@bp.app_errorhandler(500)
def error_500(error):
    return f'<h2>{error}</h2>', 500