from flask import Blueprint

bp = Blueprint('errors', __name__)

from url_shortener.errors import handlers