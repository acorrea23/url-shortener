from flask import Blueprint

bp = Blueprint('main', __name__)

from url_shortener.main import routes